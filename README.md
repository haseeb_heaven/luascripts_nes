# LuaScripts_NES
Contains Various LuaScripts for NES Games for FCEUX Emulator.

Q : How to Run these Scripts ?
A : Open FCEUX Emulator and open Desired ROM File.
Then go to FILE > Lua > New Lua Script Window > Browse > Then Select Desired Script > Then Click Run and Enjoy :)

Q : How Many Games are there ?
A : 4 Games of NES as mentioned below :
  1) Super Mario Bros.
  2) Battle City.
  3) Mighty Final Fight.
  4) Yie-Ar Kung-Fu.

Q : What library does it require to run ?
A : Some scripts use GUI which requires 'iupla' library.
and some script uses x_funtions.lua file.

you don't have to worry about downloading them because they will come pre-installed with your FCEUX Emulator :)

Q : Can i use it in my own script/project ?
A : Yes, feel free to use it or distibute it but always mention author and give credits in your project first.
